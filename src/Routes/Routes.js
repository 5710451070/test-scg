import React from "react"
import { Route, Switch } from "react-router"

import SCG from "../components/SCG/SCG"
import Main from "../components/Main/Main"
import Personal from "../components/Personal/Personal"
import EXP from "../components/EXP/EXP"


const MainRoutes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Main} />
      <Route exact path="/SCG/profile" component={Personal} />
      <Route exact path="/SCG/history" component={SCG} />
      <Route exact path="/SCG/exp" component={EXP} />
    </Switch>
  )
}

export default MainRoutes
