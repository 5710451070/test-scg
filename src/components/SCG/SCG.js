import React, { Component } from "react"
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import "./SCG.css"
import styled, { keyframes } from "styled-components"
import { bounceInDown, fadeIn, bounceInUp } from "react-animations"
import Typist from "react-typist"

const slideAnimation = keyframes`${bounceInDown}`

const fadee = keyframes`${bounceInUp}`

const Slide = styled.div`
  animation: 2s ${slideAnimation};
`

const Fade = styled.div`
  animation: 10s ${fadee};
`

class SCG extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="">
          <div
            className="d-flex justify-content-center align-items-center flex-column"
            style={{ height: "1024x" }}
          >
            <div className="row mt-3">
              <div className="col-md-3">
                <Slide>
                  {" "}
                  <img
                    className="mt-3"
                    src="/bg/1150.jpg"
                    style={{ borderRadius: "50%", height: "200px" }}
                  />
                </Slide>
              </div>

              <div className="col-md-9">
                <Typist>
                  <Typist.Delay ms={1000} />

                  <h1>Chanapai Suparbpong</h1>
                  <h1>(Flouk)</h1>
                  <h1>Junior Developer</h1>
                </Typist>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-md-6">
              <Fade>
                {" "}
                <h1 className="text-center">Contact</h1>
                <div className="pl-5">
                  <h3 className="text-left">
                    ที่อยู่ : 156/1 ซ.ประชาสงเคราะห์23 ดินแดง กรุงเทพฯ 10400
                  </h3>
                  <h3 className="text-left">เบอร์โทรศัพท์ : 090-9537447</h3>
                  <h3 className="text-left">อีเมล : flouk1150@gmail.com</h3>
                </div>
              </Fade>
            </div>

            <div className="col-md-6">
              <Fade>
                {" "}
                <h1>Education</h1>
                <div className="">
                  <h3 className="text-center">
                    Kasetsart University (Bangkhen)
                  </h3>
                  <h3 className="text-center">Banchelor of Science</h3>
                  <h3 className="text-center">Major in Computer-Science, 2018</h3>
                </div>
              </Fade>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    )
  }
}

export default SCG
