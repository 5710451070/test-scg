import React, { Component } from "react"
import "./Main.css"
import Typist from "react-typist"
import { Button } from "react-bootstrap"
import Navbar from "react-bootstrap/Navbar"
import { Link } from "react-router-dom"

class Main extends Component {
  render() {
    return (
      <div
        className="d-flex justify-content-center align-items-center bg"
        style={{ height: "100vh" }}
      >
        <Typist>
          <h1> Welcome To My CV</h1>
          <Typist.Delay ms={500} />
          <br />
          <h2 style={{ color: "gr" }}>
            ถ้าผิดพลาดประการใดขออภัยไว้ ณ ที่นี้ด้วย (เนื่องจากไม่มีเวลาทำเลย
            TT)
          </h2>
          <br />
          <br />
          <Link to="/SCG/history">
            <Button className="btn-let">Let's Go</Button>
          </Link>
        </Typist>
      </div>
    )
  }
}

export default Main
