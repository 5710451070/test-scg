import React, { Component } from "react"
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import styled, { keyframes } from "styled-components"
import {
  bounceInDown,
  fadeIn,
  bounceInUp,
  fadeInLeftBig
} from "react-animations"
import Typist from "react-typist"

const slideAnimation = keyframes`${fadeInLeftBig}`

const fadee = keyframes`${bounceInUp}`

const Slide = styled.div`
  animation: 4s ${slideAnimation};
`

const Fade = styled.div`
  animation: 10s ${fadee};
`

class Personal extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="">
          <div
            className="d-flex justify-content-center align-items-center flex-column"
            style={{ height: "1024x" }}
          >
            <Typist>
              <Typist.Delay ms={1000} />

              <h1 className="mt-5">Personal Profile</h1>
            </Typist>
            <Slide>
              <div
                className="px-1 py-3"
                style={{
                  backgroundColor: "blue",
                  width: "500px",
                  height: "300px"
                }}
              >
                <Typist>
              <Typist.Delay ms={3000} />

              <h2 className="text-left mx-3">Hi , you can call me Flouk Now I am fullstack developer at PIRSquare. I like programming and read articles about IT, Coding. My free time fix bug and learn from udemy.</h2>
            </Typist>
              </div>
            </Slide>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    )
  }
}

export default Personal
