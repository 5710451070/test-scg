import React, { Component } from "react"
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import styled, { keyframes } from "styled-components"
import {
  bounceInDown,
  fadeIn,
  bounceInUp,
  fadeInLeftBig
} from "react-animations"
import Typist from "react-typist"

const slideAnimation = keyframes`${fadeIn}`

const fadee = keyframes`${fadeIn}`

const Slide = styled.div`
  animation: 4s ${slideAnimation};
`

const Fade = styled.div`
  animation: 2s ${fadee};
`

class EXP extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="container mt-5">
        <Fade>
          <div className="row">
            <div className="col">
              <h1>ประสบการณ์</h1>
              <div style={{ marginLeft: "50px" }}>
                <div className="text-left">
                  <h6 className="font-weight-bold"><u>PIRSquare</u></h6>
                  <h6>Nov 2018 - Now</h6>
                  <p>- Develop Web front End && BackEnd</p>
                  <p>- Do everything according requirement</p>
                  <p>- Fix bug as a hobby</p>
                </div>
                <div className="text-left">
                  <h6 className="font-weight-bold"><u>AutoPair (Intern)</u></h6>
                  <h6>Oct 2018 - Nov 2018</h6>
                  <p>- Develop Web Back office</p>
                  <p>- Develop web by jquery and koa framework</p>
                </div>
                <div className="text-left">
                  <h6 className="font-weight-bold"><u>AIS (outsource)</u> </h6>
                  <h6>JUN 2018 - SEP 2018</h6>
                  <p>- Create API backend</p>
                  <p>- Update Code front-end Angular</p>
                </div>
              </div>
            </div>
            <div className="col">
              <h1>ทักษะ</h1>
              <ul className="text-left" style={{ marginLeft: "20%" }}>
                <li>React, redux, mobx, hook, next.js</li>
                <li>Node.js, Nest.js, Koa, Mysql, Firebase</li>
                <li>Bootstrap, material UI, AntD, PrimeReact</li>
                <li>docker</li>
              </ul>
            </div>
          </div>
          </Fade>
          {/* <div
            className="d-flex justify-content-center align-items-center flex-column"
            style={{ height: "1024x" }}
          >
            <Typist>
              <Typist.Delay ms={1000} />

              <h1 className="mt-5">Personal Profile</h1>
            </Typist>
            <Slide>
              <div
                className="px-1 py-3"
                style={{
                  backgroundColor: "blue",
                  width: "500px",
                  height: "300px"
                }}
              >
                <Typist>
              <Typist.Delay ms={3000} />

              <h2 className="text-left mx-3">Hi , you can call me Flouk Now I am fullstack developer at PIRSquare. I like programming and read articles about IT, Coding. My free time fix bug and learn from udemy.</h2>
            </Typist>
              </div>
            </Slide>
          </div> */}
        </div>
        <Footer />
      </React.Fragment>
    )
  }
}

export default EXP
