import React, { Component } from "react"
import { Navbar, Nav } from "react-bootstrap"
import { NavLink } from "react-router-dom"

class Header extends Component {
  render() {
    return (
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand as={NavLink} to="/">หน้าแรก</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/SCG/history">ประวัติ</Nav.Link>
            <Nav.Link as={NavLink} to="/SCG/profile">ข้อมูลส่วนตัว</Nav.Link>
            <Nav.Link as={NavLink} to="/SCG/exp">ประสบการณ์</Nav.Link>

          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default Header
