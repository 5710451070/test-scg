import React, { Component } from "react"
import { Navbar, Nav } from "react-bootstrap"
import { NavLink } from "react-router-dom"
class Footer extends Component {
  render() {
    return (
      <Navbar style={{position : "absolute" , bottom : 0 , width : "100%"}} expand="lg" bg="dark" variant="dark">
        <Navbar.Brand as={NavLink} to="/">หน้าแรก</Navbar.Brand>

      </Navbar>
    )
  }
}

export default Footer
