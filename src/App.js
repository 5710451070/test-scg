import React from 'react';
import logo from './logo.svg';
import './App.css';

import MainRoute from "./Routes/Routes"

function App() {
  return (
    <div className="App">
      <MainRoute />
    </div>
  );
}

export default App;
